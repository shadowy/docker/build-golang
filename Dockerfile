FROM golang:1.22.4
ARG PROTOC_VERSION=27.2
ARG GO_LINT=v1.45.2
RUN which ssh-agent
RUN apt-get update -y && apt-get install openssh-client git make unzip -y
RUN eval $(ssh-agent -s)
RUN curl -L https://github.com/protocolbuffers/protobuf/releases/download/v$PROTOC_VERSION/protoc-$PROTOC_VERSION-linux-x86_64.zip --output protoc.zip
RUN unzip protoc.zip -d /usr
RUN rm protoc.zip
#RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin $GO_LINT
#RUN env GO111MODULE=auto go get -u github.com/golang/protobuf/protoc-gen-go
#RUN env GO111MODULE=auto go get github.com/gojp/goreportcard/cmd/goreportcard-cli
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
#RUN env GO111MODULE=auto go get -u github.com/swaggo/swag/cmd/swag
COPY ./script/init-private-rep.sh /bin
RUN chmod 777 /bin/init-private-rep.sh

