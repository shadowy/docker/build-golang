# CHANGELOG

<!--- next entry here -->

## 0.9.1
2023-12-15

### Fixes

- add image for grpc (add dart) (ff97958cc27b368638c0bcfd2ccde1337103ae02)

## 0.9.0
2023-12-15

### Features

- add image for grpc (add dart) (10989fa58954a141235648ef89a9d6f04ffdfec8)

## 0.8.0
2023-12-11

### Features

- update versions (c162cc71988c4db614aebdcfc00a9ee8d3dca153)

## 0.7.0
2022-09-19

### Features

- update version (6b7404b954693a19293134e7e1221c21fe779581)

## 0.6.0
2022-05-09

### Features

- update version (44aae4129abfee46cf0119a72e8f1395ed2190b2)

## 0.5.0
2022-05-09

### Features

- update version (b9e1e0801aa8bb9a6284081fdce3feda6d549b52)

## 0.4.0
2022-05-09

### Features

- update version (a82ceac56705098e1f6bac0e10c2737c95b20bda)

## 0.3.0
2022-05-09

### Features

- update version (c0ae68fbec1a0c288349cc2e16a9295f19e8dd4e)

## 0.2.3
2021-12-02

### Fixes

- update dockerfile (6474d0516afa89e4e64b75805ee58243f5c4e438)

## 0.2.2
2021-12-02

### Fixes

- update dockerfile (159221d88790205862e99f1ff75eac176cc9afd5)

## 0.2.1
2021-12-01

### Fixes

- update dockerfile (136ef3e9c161a54cecf8878268da58ce47ffc69a)

## 0.2.0
2021-12-01

### Features

- work with private repositories (cc692ab5e3be5921c7293a9c860c9b45ea5c23cb)

## 0.1.4
2021-12-01

### Fixes

- update ci/cd (ceedabfad7bd930ff34dc4d51c7e390e9d0bf176)

## 0.1.3
2021-12-01

### Fixes

- update ci/cd (70314e0f6feb3a81a38d4c30120dc9dba6e905c0)

## 0.1.2
2021-12-01

### Fixes

- update ci/cd (b15066b7781eb751c45712f2ee59259618907aa7)

## 0.1.1
2021-12-01

### Fixes

- update ci/cd (b308a238ec340aee993d0aaab29cab3ce9bfde8c)

## 0.1.0
2021-12-01

### Features

- initial commit (0af3a1ae6f79df7cea8f50da008b3282e1d6a883)