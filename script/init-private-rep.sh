#!/bin/bash
if [ -z ${GO_GITLAB_USER} ]; then
  echo "Without private repository"
else
  echo "With private repository"
  echo "machine gitlab.com login ${GO_GITLAB_USER} password ${GO_GITLAB_PASSWORD}" > ~/.netrc
fi
